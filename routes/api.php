<?php

use App\Http\Controllers\API\AdminUsersAPIController;
use App\Http\Controllers\API\AuthAPIController;
use App\Http\Controllers\API\ChatAPIController;
use App\Http\Controllers\API\GroupAPIController;
use App\Http\Controllers\API\PasswordResetController;
use App\Http\Controllers\API\RoleAPIController;
use App\Http\Controllers\API\SocialAuthAPIController;
use App\Http\Controllers\API\UserAPIController;
use App\Http\Controllers\API\BlockUserAPIController;
use App\Http\Controllers\API\NotificationController;
use App\Http\Controllers\API\FireBaseController;
use Illuminate\Broadcasting\BroadcastController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// User Login API
Route::post('/login', [AuthAPIController::class, 'login']);
Route::post('/register', [AuthAPIController::class, 'register']);
Route::post('password/reset', [PasswordResetController::class, 'sendResetPasswordLink']);
Route::post('password/update', [PasswordResetController::class, 'reset']);
Route::get('activate', [AuthAPIController::class, 'verifyAccount']);

Route::middleware(['auth:api', 'user.activated'])->group(function () {
    Route::post('broadcasting/auth', [BroadcastController::class, 'authenticate']);
    Route::get('logout', [AuthAPIController::class, 'logout']);

    //get all user list for chat
    Route::get('users-list', [UserAPIController::class, 'getUsersList']);
    Route::get('get-users', [UserAPIController::class, 'getUsers'])->name('get-users')->name('get-users');
    Route::delete('remove-profile-image',
        [UserAPIController::class, 'removeProfileImage'])->name('remove-profile-image');
    /** Change password */
    Route::post('change-password', [UserAPIController::class, 'changePassword'])->name('change-password');
    Route::get('conversations/{ownerId}/archive-chat', [UserAPIController::class, 'archiveChat'])->name('conversations.archive-chat');
    Route::get('conversations/{ownerId}/un-archive-chat', [UserAPIController::class, 'unArchiveChat'])->name('conversations.un-archive-chat');

    Route::get('get-profile', [UserAPIController::class, 'getProfile']);
    Route::post('profile', [UserAPIController::class, 'updateProfile'])->name('update.profile');
    Route::post('update-last-seen', [UserAPIController::class, 'updateLastSeen'])->name('update-last-seen');

    Route::post('send-message',
        [ChatAPIController::class, 'sendMessage'])->name('conversations.store')->middleware('sendMessage');
    Route::get('users/{id}/conversation', [UserAPIController::class, 'getConversation'])->name('users.conversation');
    Route::get('conversations', [ChatAPIController::class, 'getLatestConversations'])->name('conversations-list');
    Route::get('archive-conversations', [ChatAPIController::class, 'getArchiveConversations'])->name('archive-conversations');
    Route::post('read-message', [ChatAPIController::class, 'updateConversationStatus'])->name('read-message');
    Route::post('file-upload', [ChatAPIController::class, 'addAttachment'])->name('file-upload');
    Route::post('image-upload', [ChatAPIController::class, 'imageUpload'])->name('image-upload');
    Route::get('conversations/{userId}/delete', [ChatAPIController::class, 'deleteConversation'])->name('conversations.destroy');
    Route::post('conversations/message/{conversation}/delete', [ChatAPIController::class, 'deleteMessage'])->name('conversations.message-conversation.delete');
    Route::post('conversations/{conversation}/delete', [ChatAPIController::class, 'deleteMessageForEveryone']);
    Route::get('/conversations/{conversation}', [ChatAPIController::class, 'show']);
    Route::post('send-chat-request', [ChatAPIController::class, 'sendChatRequest'])->name('send-chat-request');
    Route::post('accept-chat-request',
        [ChatAPIController::class, 'acceptChatRequest'])->name('accept-chat-request');
    Route::post('decline-chat-request',
        [ChatAPIController::class, 'declineChatRequest'])->name('decline-chat-request');

    /** Web Notifications */
    Route::put('update-web-notifications', [UserAPIController::class, 'updateNotification'])->name('update-web-notifications');

    /** BLock-Unblock User */
    Route::put('users/{user}/block-unblock', [BlockUserAPIController::class, 'blockUnblockUser'])->name('users.block-unblock');
    Route::get('blocked-users', [BlockUserAPIController::class, 'blockedUsers']);

    /** My Contacts */
    Route::get('my-contacts', [UserAPIController::class, 'myContacts'])->name('my-contacts');

    /** Groups API */
    Route::post('groups', [GroupAPIController::class, 'create'])->name('groups.create');
    Route::post('groups/{group}', [GroupAPIController::class, 'update'])->name('groups.update');
    Route::get('groups', [GroupAPIController::class, 'index'])->name('groups.index');
    Route::get('groups/{group}', [GroupAPIController::class, 'show'])->name('group.show');
    Route::put('groups/{group}/add-members', [GroupAPIController::class, 'addMembers'])->name('groups-group.add-members');
    Route::delete('groups/{group}/members/{user}', [GroupAPIController::class, 'removeMemberFromGroup'])->name('group-from-member-remove');
    Route::delete('groups/{group}/leave', [GroupAPIController::class, 'leaveGroup'])->name('groups.leave');
    Route::delete('groups/{group}/remove', [GroupAPIController::class, 'removeGroup'])->name('group-remove');
    Route::put('groups/{group}/members/{user}/make-admin', [GroupAPIController::class, 'makeAdmin'])->name('groups.members.make-admin');
    Route::put('groups/{group}/members/{user}/dismiss-as-admin', [GroupAPIController::class, 'dismissAsAdmin'])->name('groups.members.dismiss-as-admin');
    Route::get('users-blocked-by-me', [BlockUserAPIController::class, 'blockUsersByMe']);

    Route::get('notification/{notification}/read', [NotificationController::class, 'readNotification'])->name('notification.read-notification');
    Route::get('notification/read-all', [NotificationController::class, 'readAllNotification'])->name('read-all-notification');
    Route::get('notification', [NotificationController::class, 'getNotifications'])->name('getNotifications');

    Route::put('update-player-id', [UserAPIController::class, 'updatePlayerId'])->name('update-player-id');
    //set user custom status route
    Route::post('set-user-status', [UserAPIController::class, 'setUserCustomStatus'])->name('set-user-status');
    Route::get('clear-user-status', [UserAPIController::class, 'clearUserCustomStatus'])->name('clear-user-status');

    //report user
    Route::post('report-user', [ReportUserController::class, 'store'])->name('report-user.store');


    /** Update Web-push */
    Route::put('update-web-notifications', [UserAPIController::class, 'updateNotification']);

    /** create group **/
    Route::post('groups', [GroupAPIController::class, 'create'])->name('create-group');
    Route::put('groups/{group}/add-members', [GroupAPIController::class, 'addMembers'])->name('groups-group.add-members');
    /** Social Login */
    Route::post('social-login/{provider}', [SocialAuthAPIController::class, 'socialLogin'])->middleware('web');

    Route::patch('/fcm-token', [FireBaseController::class, 'updateToken'])->name('fcmToken');
    Route::post('send-notification',[FireBaseController::class,'notification'])->name('notification');

});

Route::middleware(['role:Admin', 'auth:api', 'user.activated'])->group(function () {
    Route::resource('users', AdminUsersAPIController::class);
    Route::post('users/{user}/update', [AdminUsersAPIController::class, 'update']);
    Route::post('users/{user}/active-de-active', [AdminUsersAPIController::class, 'activeDeActiveUser'])
        ->name('active-de-active-user');

    Route::resource('roles', RoleAPIController::class);
    Route::post('roles/{role}/update', [RoleAPIController::class, 'update']);
});

Route::post('webhook', [UserAPIController::class, 'webHook']);
