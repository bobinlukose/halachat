<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\SendPushNotification;
use Illuminate\Http\Request;
use Kutia\Larafirebase\Facades\Larafirebase;
use App\Http\Controllers\AppBaseController;
use Illuminate\Notifications\Notification;
use App\Repositories\NotificationRepository;
use Illuminate\Http\JsonResponse;
use Kreait\Laravel\Firebase\Facades\Firebase;



class FireBaseController extends AppBaseController
{
    private $notification;
    public function __construct()
    {
        $this->notification = Firebase::messaging();
    }


    public function setToken(Request $request)
    {
        $token = $request->input('fcm_token');
        $request->user()->update([
            'fcm_token' => 'ihasbdjkasnd'
        ]); //Get the currrently logged in user and set their token

        return $this->sendResponse([], 'Successfully Updated FCM Token');


    }


    public function updateToken(Request $request){
        try{
            $request->user()->update(['fcm_token'=>$request->token]);
            return response()->json([
                'success'=>true
            ]);
        }catch(\Exception $e){
            report($e);
            return response()->json([
                'success'=>false
            ],500);
        }
    }
    public function notification(Request $request){
        $request->validate([
            'title'=>'required',
            'message'=>'required'
        ]);

        try{

            $fcmTokens = User::whereNotNull('fcm_token')->pluck('fcm_token')->toArray();

           // Notification::send(null,new SendPushNotification($request->title,$request->message,$fcmTokens));

            /* or */

           // auth()->user()->notify(new SendPushNotification($request->title,$request->message,$fcmTokens));

            /* or */

           $data =  Larafirebase::withTitle($request->title)
                ->withBody($request->message)
                ->sendMessage($fcmTokens);
            return $this->sendResponse([], 'Notification Sent Successfully!!');
        }catch(\Exception $e){
            report($e);
            return redirect()->back()->with('error','Something goes wrong while sending notification.');
        }
    }
}
