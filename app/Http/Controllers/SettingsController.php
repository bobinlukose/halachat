<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiOperationFailedException;
use App\Http\Requests\UpdateSettingRequest;
use App\Models\Chatpermission;
use App\Models\Role;
use App\Repositories\SettingsRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Laracasts\Flash\Flash;

/**
 * Class SettingsController
 */
class SettingsController extends AppBaseController
{
    /**
     * @var SettingsRepository
     */
    private $settingsRepository;

    /**
     * SettingsController constructor.
     *
     * @param  SettingsRepository  $settingsRepository
     */
    public function __construct(SettingsRepository $settingsRepository)
    {
        $this->settingsRepository = $settingsRepository;
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $settings = $this->settingsRepository->getSettings();
        $enabledGroupChat = 'checked';
        $membersCanAddGroup = 'checked';

        if (isset($settings['enable_group_chat'])) {
            $enabledGroupChat = ($settings['enable_group_chat'] == 1) ? 'checked' : '';
        }

        if (isset($settings['members_can_add_group'])) {
            $membersCanAddGroup = ($settings['members_can_add_group'] == 1) ? 'checked' : '';
        }

        return view('settings.index', compact('settings', 'enabledGroupChat', 'membersCanAddGroup'));
    }

    /**
     * @param  UpdateSettingRequest  $request
     * @return RedirectResponse
     *
     * @throws ApiOperationFailedException
     */
    public function update(UpdateSettingRequest $request): RedirectResponse
    {
        $this->settingsRepository->updateSettings($request->all());
        Flash::success('Settings updated successfully.');

        return redirect()->route('settings.index');
    }


    /**
     * @return Factory|View
     */
    public function chatSettings()
    {
        $settings = $this->settingsRepository->getSettings();
        $enabledGroupChat = 'checked';
        $membersCanAddGroup = 'checked';

        if (isset($settings['enable_group_chat'])) {
            $enabledGroupChat = ($settings['enable_group_chat'] == 1) ? 'checked' : '';
        }

        if (isset($settings['members_can_add_group'])) {
            $membersCanAddGroup = ($settings['members_can_add_group'] == 1) ? 'checked' : '';
        }

        $roles = Role::all();

        return view('settings.chatsettings', compact('settings', 'enabledGroupChat', 'membersCanAddGroup' , 'roles'));
    }



    /**
     * @param  UpdateSettingRequest  $request
     * @return RedirectResponse
     *
     * @throws ApiOperationFailedException
     */
    public function chatSettingsUpdate(Request $request)
    {
        $req = $request->all();
        $role_from = $req['role_from'];
        Chatpermission::where('chat_from_roleid', $role_from)->delete();

        if(isset($req['role_to'])){
            foreach ($req['role_to'] as $val){
                $user = new Chatpermission();
                $user->chat_from_roleid = $role_from;
                $user->chat_to_roleid = $val;
                $user->status = 1;
                $user->save();
            }
        }


        Flash::success('Chat settings updated successfully.');

        return redirect()->route('settings.chatSettings');
        /*        return view('settings.chatsettings', compact('settings', 'enabledGroupChat', 'membersCanAddGroup' , 'roles'));*/


    }


    public function getChatPermissions(Request $request){
        $id = $request->query('id');
        $chatPermissions = $this->settingsRepository->getChatPermissionsOfRoleId($id);
        // Return a JSON response
        return response()->json($chatPermissions);

    }
}
