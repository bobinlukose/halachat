<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SearchUsers extends Component
{
    public $users = [];

    public $myContactIds = [];

    public $searchTerm;

    public $male;

    public $female;

    protected $listeners = ['clearSearchUsers' => 'clearSearchUsers'];

    /**
     * @param  array  $ids
     */
    public function setMyContactIds($ids)
    {
        $this->myContactIds = $ids;
    }

    /**
     * @return array
     */
    public function getMyContactIds()
    {
        return $this->myContactIds;
    }

    /**
     * initialize variables
     *
     * @param $myContactIds
     * @param $blockUserIds
     */
    public function mount($myContactIds, $blockUserIds)
    {
        $userIds = array_unique(array_merge($blockUserIds, array_keys($blockUserIds)));
        $userIds = array_unique(array_merge($userIds, $myContactIds));
        $this->setMyContactIds($userIds);
    }

    /**
     * @return Application|Factory|View
     */
    public function render()
    {
        $this->searchUsers();

        return view('livewire.search-users');
    }

    public function clearSearchUsers()
    {
        $this->male = false;
        $this->female = false;
        $this->searchTerm = '';

        $this->searchUsers();
    }

    /**
     * search users and apply filters
     */
    public function searchUsers()
    {
        $male = $this->male;
        $female = $this->female;
        if ($this->male && $this->female) {
            $male = false;
            $female = false;
        }


        $roleName = Auth::user()->roles->pluck('name')->toArray();
        $roleId = DB::table('roles')
            ->select('id')
            ->where('name','=',$roleName[0])
            ->get()->toArray();


        $permittedRoleNames = DB::table('chat_permission')
            ->join('roles', 'chat_permission.chat_to_roleid', '=', 'roles.id')
            ->select('roles.name')
            ->where('chat_permission.chat_from_roleid','=',$roleId[0]->id)
            ->get()->toArray();
        $permittedRoleNames = array_column($permittedRoleNames, 'name');


        $users = User::whereNotIn('id', $this->getMyContactIds())->role($permittedRoleNames)
            ->when($male, function ($query) {
                return $query->where('gender', '=', User::MALE);
            })
            ->when($female, function ($query) {
                return $query->where('gender', '=', User::FEMALE);
            })
            ->when($this->searchTerm, function ($query) {
                return $query->where(function ($q) {
                    $q->whereRaw('name LIKE ?', ['%'.strtolower($this->searchTerm).'%'])
                    ->orWhereRaw('email LIKE ?', ['%'.strtolower($this->searchTerm).'%']);
                });
            })
            ->orderBy('name', 'asc')
            ->select(['id', 'is_online', 'gender', 'photo_url', 'name', 'email'])
            ->limit(20)
            ->get()
            ->except(getLoggedInUserId());

        $this->users = $users;
    }
}
