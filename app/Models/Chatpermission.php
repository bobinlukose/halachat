<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chatpermission extends Model
{
    // Define the table associated with this model (optional, by default it infers the table name from the model's class name)
    // protected $table = 'products';

    /**
     * @var string
     */
    protected $table = 'chat_permission';

    // Define the primary key column name (optional, by default it assumes 'id')
    // protected $primaryKey = 'id';

    // Define the fillable columns that can be mass-assigned (i.e., through create() or update() methods)
    protected $fillable = ['chat_from_roleid', 'chat_to_roleid', 'status'];

    // Define the guarded columns that cannot be mass-assigned (optional)
    // protected $guarded = [];

    // Timestamps: Laravel automatically handles created_at and updated_at columns if they exist (optional)
    // public $timestamps = true;

    // If you don't want timestamps, set the value to false
    // public $timestamps = false;
}
