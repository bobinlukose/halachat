require('./bootstrap');
import Echo from 'laravel-echo';
window.Pusher = require('pusher-js');
import Ably from 'ably';
window.Pusher = require('pusher-js');
window.Ably = Ably;
if(process.env.MIX_BROADCAST_DRIVER =='ably'){
    console.log(process.env.MIX_BROADCAST_DRIVER);
    window.Echo = new Echo({
        broadcaster: 'pusher',
        key: process.env.MIX_ABLY_KEY,
        wsHost: 'realtime-pusher.ably.io',
        wsPort: 443,
        disableStats: true,
        encrypted: true,
        cluster: '',
        auth: {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
        },
    });
  /*  window.Echo.channel('my-channel').listen('my-event', (data) => {
        console.log(data);
    });*/

}else{
    console.log(process.env.MIX_BROADCAST_DRIVER);
    window.Echo = new Echo({
        broadcaster: 'pusher',
        key: pusherKey,
        cluster: pusherCluster,
        encrypted: true,
        auth: {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
        },
    });
}



