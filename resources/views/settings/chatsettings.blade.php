@extends('layouts.app')
@section('title')
    {{ __('messages.chatsettings') }}
@endsection
@section('page_css')
    <link rel="stylesheet" href="{{ mix('assets/css/admin_panel.css') }}">
@endsection
@section('content')
    <div class="container-fluid page__container">
        <div class="animated fadeIn main-table">
            @include('flash::message')
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header page-header">
                            <div class="pull-left page__heading my-2">
                                {{ __('messages.chatsettings') }}
                            </div>
                        </div>
                        <div class="card-body">
                            @include('coreui-templates::common.errors')
                            <form method="post" id="settingForm" action="{{ route('chatSettings.update') }}">
                                {{ csrf_field() }}



                                <div class="filter-container user-filter align-self-sm-center align-self-end ms-auto">
                                    <div class="me-2 my-2 user-select2 ms-sm-0 ms-auto">
                                        {{--<div class=""><label class="login-group__sub-title">{{ 'Select Role' . ':' }}</label></div>--}}
                                        <select name="role_from" id="role_from" class="form-control">
                                            <option value="">Select a Role</option>
                                            @foreach ($roles as $role)
                                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>


                                <br/>
                                <br/>

                                <table>
                                    @foreach ($roles as $role)
                                        {{--<div class="row">
                                            <div class="col-md-6">
                                                <!-- Is Active Field -->
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        --}}{{--<div class=""><label class="login-group__sub-title">{{ $role->name }}</label></div>--}}{{--
                                                        <label class="login-group__sub-title">{{ $role->name }}</label>
                                                        <label class="switch switch-label switch-outline-primary-alt">
                                                            <input name="enable_group_chat" class="switch-input enable_group_chat not-checkbox"
                                                                   type="checkbox" value="{{ $role->id }}">
                                                            <span class="switch-slider" data-checked="&#x2713;"
                                                                  data-unchecked="&#x2715;"></span>
                                                        </label>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>--}}

                                        <div class="form-check form-check-inline class_role_to_{{ $role->id }} class_role_to_common">
                                            {{--<input class="form-check-input" type="checkbox" value="{{ $role->id }}" id="flexCheckDisabled" />--}}

                                            <tr>
                                                <td><label class="form-check-label" style="padding: 30px;" for="flexCheckDisabled">{{ $role->name }}:</label></td>
                                                <td><label class="switch switch-label switch-outline-primary-alt">
                                                        <input name="role_to[]" id="role_to_{{ $role->id }}" class="switch-input enable_group_chat not-checkbox chatPermRoles" type="checkbox" value="{{ $role->id }}">
                                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                                    </label></td>
                                            </tr>




                                        </div>
                                        <br/>
                                    @endforeach
                                </table>











                                <div class="form-group mt-3">
                                    {{ Form::button(__('messages.save') , ['type'=>'submit','class' => 'btn btn-primary me-1','id'=>'btnSave','data-loading-text'=>"<span class='spinner-border spinner-border-sm'></span> " .__('messages.processing')]) }}
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ mix('assets/js/admin/users/edit_user.js') }}"></script>
@endsection
