<!DOCTYPE html>
<html lang="en">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>{{getAppName()}}</title>
    <meta name="description"
          content="">
    <meta name="keyword" content="CoreUI,Bootstrap,Admin,Template,InfyOm,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <!-- PWA  -->
    <meta name="theme-color" content="#009ef7"/>
    <link rel="apple-touch-icon" href="{{ asset('assets/images/logo-30x30.png') }}">
    <link rel="manifest" href="{{ asset('manifest.json') }}">
    <!-- favicon -->
    <link rel="shortcut icon" href="{{getFaviconUrl()}}">
    <link rel="stylesheet" href="{{ mix('assets/css/bootstrap.min.css') }}">
    <!-- google font -->
    <link href="//fonts.googleapis.com/css?family=Poppins:400,500,700&display=swap" rel="stylesheet">
    <!-- font awesome version 4.7 -->
    <link rel="stylesheet" href="{{ mix('assets/css/font-awesome.css') }}">
    <!-- custom css -->
    <link rel="stylesheet" href="{{ mix('assets/css/landing-page-style.css') }}">
</head>
<body>
<!--header start-->
<header class="header">
    <div class="header__container container d-flex align-items-center">
        <div class="header__logo-brand">
            <img src="{{ getLogoUrl() }}" alt="logo-image" class="header__logo">
            <span class="header__brand-name">{{getAppName()}}</span>
        </div>
        <div class="header__bar">
            <i class="fa fa-bars" aria-hidden="true" id="barIcon"></i>
        </div>
        <div class="header__collapsible ms-auto align-items-center" id="collapsedNav">
            <nav>
                @auth
                    @if (\Auth::check() && \Auth::user()->hasPermissionTo('manage_conversations'))
                        <a href="{{ url('/conversations') }}" class="header__link">{{ __('messages.chat') }}</a>
                    @elseif (\Auth::check())
                        @if(\Auth::user()->getAllPermissions()->count() > 0)
                            @php
                                $url = getPermissionWiseRedirectTo(\Auth::user()->getAllPermissions()->first())
                            @endphp
                            <a href="{{ url($url) }}" class="header__link">{{ __('messages.chat') }}</a>
                        @else
                            <a href="{{ url('/conversations') }}" class="header__link">{{ __('messages.chat') }}</a>
                        @endif
                    @endif
                @else
                    @if (Route::has('login'))
                        <a href="{{ route('login') }}" class="header__link">{{ __('messages.login') }}</a>
                    @endif
                    @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="header__link">{{ __('messages.register') }}</a>
                    @endif
                @endauth
            </nav>
        </div>
    </div>
</header>
<!--header over-->
<!--landing section-->
<section class="landing d-flex align-items-center">
    <div class="landing__illustration">
        <img src="{{ !empty(getFrontCmsImage('landing_image')) ? getFrontCmsImage('landing_image') : asset('assets/images/chat-illustrator.png')}}"
             alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12">
                <h1 class="landing__heading fw-bolder mb-3">{{ getAppName() }}</h1>


                <a href="{{ route('register') }}" class="landing__get-start-btn primary-btn btn text-white">
                    {{ __('messages.get_started') }}
                </a>
            </div>
            <div class="col-12 landing__responsive-col">
                <img src="{{ !empty(getFrontCmsImage('landing_image')) ? getFrontCmsImage('landing_image') : asset('assets/images/chat-illustrator.png')}}"
                     alt="landing image" class="img-fluid">
            </div>
        </div>
    </div>
</section>
<!--feature section-->

<!--/ start-using -->
<!--footer-->
<footer class="footer">

</footer>
<!--/footer-->
<!-- script for custom mobile navigation -->
@routes
<script src="{{ asset('sw.js') }}"></script>
<script>
    let barIcon = document.getElementById('barIcon')
    let headerCollapsible = document.getElementById('collapsedNav')

    barIcon.onclick = function () {
        if (headerCollapsible.classList.contains('open')) {
            headerCollapsible.classList.remove('open')
        } else {
            headerCollapsible.className += ' open'
        }
    }
    if (!navigator.serviceWorker.controller) {
        navigator.serviceWorker.register('/sw.js').then(function (reg) {
            console.log('Service worker has been registered for scope: ' + reg.scope)
        })
    }
</script>
</body>
</html>
