# Use the official PHP 8.1 FPM image as the base image
FROM php:8.1-fpm

# Set the working directory inside the container
WORKDIR /var/www

# Install dependencies
# Install system dependencies
RUN apt-get update && apt-get install -y zlib1g-dev zlib1g \
    build-essential \
    locales \
    libpq-dev \
    pkg-config \
    libzstd-dev \
    libzip-dev \
    libpcre3-dev libpcre3 \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip \
    libcurl4 \
    libcurl4-openssl-dev \
    libmcrypt-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*




#enabling php extensions

RUN docker-php-ext-install pdo pdo_pgsql mbstring curl
RUN docker-php-ext-install pdo_mysql exif bcmath gd zip sockets
RUN docker-php-ext-install calendar


# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

ENV COMPOSER_ALLOW_SUPERUSER=1

# Copy the Laravel project files to the working directory
COPY . .

COPY ./docker/php-fpm.d/www.conf /usr/local/etc/php-fpm.d/www.conf
COPY ./docker/php.conf/php.ini /usr/local/etc/php/php.ini

# Set permissions for Laravel storage and bootstrap cache folders
RUN chown -R www-data:www-data /var/www/storage /var/www/bootstrap/cache
RUN chmod -R 775 /var/www/storage /var/www/bootstrap/cache

# Install project dependencies using Composer
RUN composer install --no-interaction --optimize-autoloader --no-dev

# Generate Laravel application key
RUN php artisan key:generate
RUN php artisan config:cache
RUN php artisan cache:clear
RUN php artisan config:clear


# Expose port 8000 and start php-fpm server
EXPOSE 8000
CMD ["php-fpm"]
