importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js');

firebase.initializeApp({
    apiKey: "AIzaSyAYpu4P-uIoVXTywotNxbPrZp04xhqfz1c",
    projectId: "halachat-2fbd9",
    messagingSenderId: "285558058439",
    appId: "1:285558058439:web:22bfda354e2e75135afde2",
});

const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function({data:{title,body,icon}}) {
    return self.registration.showNotification(title,{body,icon});
});
